/* 
 * Filename     : file_writer.h
 * Description  : Class for writing logs into a file.
 * Maintainer   : Christophe Burki
 * Version      : 1.0.0
 * 
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_FILEWRITER
#define BO_FILEWRITER

/* -------------------------------------------------------------------------- */

#include <string>
#include <fstream>
#include "writer.h"

using std::string;
using std::ofstream;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Writer class for writing into a file.
 */
class FileWriter : public Writer {

 private :
    ofstream logFile;
    
 public :
    FileWriter(const string filename, const bool append = true);
    ~FileWriter(void);
    void write(const string msg);
    void write(const string msg, const string name, const LogLevel level, const string date, const Location location);
};

/* -------------------------------------------------------------------------- */

#endif
