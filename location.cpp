// 
// Filename     : location.cpp
// Description  : Class defining the log location.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include "location.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 */
Location::Location(void) {
    
    this->filename = "";
    this->funcname = "";
    this->line =  0;
}


/**
 * @~english
 * @brief Constructor
 *
 * @param filename The name of the file where the log is generated.
 * @param funcname The name of the function where the log is generated.
 * @param line The line number where the log is generated.
 */
Location::Location(const string filename, const string funcname, const unsigned long line) {
    
    this->filename = filename;
    this->funcname = funcname;
    this->line =  line;
}

/**
 * @~english
 * @brief Destructor
 */
Location::~Location(void) {
    
}

/**
 * @~english
 * @brief Return the name of the file.
 *
 * @return The name of the file.
 */
string Location::getFilename(void) const {
    
    return this->filename;
}

/**
 * @~english
 * @brief Return the name of the function.
 *
 * @return The name of the function.
 */
string Location::getFuncname(void) const {
    
    return this->funcname;
}


/**
 * @~english
 * @brief Return the line number.
 *
 * @return The line number.
 */
unsigned long Location::getLine(void) const {

    return this->line;
}

/* -------------------------------------------------------------------------- */
