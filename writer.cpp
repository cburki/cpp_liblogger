// 
// Filename     : writer.cpp
// Description  : Abstract for writing logs.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include "writer.h"

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Set the format for the message.brief
 *
 * @param format The format to set.
 */
void Writer::setFormat(const string format) {

    this->format = format;
}


/**
 * @~english
 * @brief Return the configured format.
 *
 * @return The configured format or the default one if none is configured.
 */
string Writer::getFormat(void) {

    return format;
}


/**
 * @~english
 * @brief Set the date and time format.
 *
 * @param format The date and time format to set.
 */
void Writer::setDatetimeFormat(const string format) {

    datetimeFormat = format;
}


/**
 * @~english
 * @brief Return a formatted datetime according to the format.
 *
 * @return The formatted datetime.
 */
string Writer::getDatetime(void) {

    time_t now = time(NULL);
    struct tm *timeinfo;
    char timeBuffer[64];
    string datetime = "";

    if (datetimeFormat.length() != 0) {
        timeinfo = localtime(&now);
        strftime(timeBuffer, 64, datetimeFormat.c_str(), timeinfo);
        datetime = string(timeBuffer);
    }

    return datetime;
}

/* -------------------------------------------------------------------------- */
