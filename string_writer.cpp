// 
// Filename     : string_writer.cpp
// Description  : Class for writing logs into a string.
// Maintainer   : Christophe Burki
// Version      : 1.0.0
// 
// 
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 
// 
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include "string_writer.h"
#include "level.h"
#include "location.h"
#include "formatter.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english Constructor
 * @brief
 */
StringWriter::StringWriter(string *str) {

    this->format = defaultFormat;
    this->datetimeFormat = defaultDatetimeFormat;
    this->str = str;
}


/**
 * @~english Destructor
 * @brief
 */
StringWriter::~StringWriter(void) {
}


/**
 * @~english
 * @brief Writer the log message into a string.
 *
 * @param msg The message to write into the string.
 */
void StringWriter::write(const string msg) {

    this->str->assign(msg);
}


/**
 * @~english
 * @brief Writer the log message into a string.
 *
 * @param msg The message to write to the console.
 * @param name The name of the logger.
 * @param level The log level.
 * @param date The date.
 * @param location The message location.
 */
void StringWriter::write(const string msg, const string name, const LogLevel level, const string date, const Location location) {

    Formatter formatter(format);
    write(formatter.format(msg, name, level, date, location));
}

/* -------------------------------------------------------------------------- */
