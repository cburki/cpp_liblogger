// 
// Filename     : logger.cpp
// Description  : Class defining a logger.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <iostream>
#include <string>
#include <time.h>
#include <ctime>

#include "logger.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 *
 * @param level
 * @param namespace
 * @param writer
 */
Logger::Logger(const LogLevel level, const string name, Writer *writer) {
    
    this->level = level;
    this->name = name;
    this->writer = writer;
}


/**
 * @~english
 * @brief Write the log message using the writer.
 *
 * @param level The log level.
 * @param msg The log message to write.
 */
void Logger::write(const LogLevel level, const string msg) {

    writer->write(msg, name, level, writer->getDatetime(), Location());
}


/**
 * @~english
 * @brief Write the log message using the writer.
 *
 * @param level The log level.
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::write(const LogLevel level, const string msg, const Location location) {

    writer->write(msg, name, level, writer->getDatetime(), location);
}


/**
 * @~english
 * @brief Log the message.
 *
 * @param level The log level to set.
 * @param msg The log message to write.
 */
void Logger::log(const LogLevel level, const string msg) {

    if (this->level <= level) {

        write(level, msg);
    }
}


/**
 * @~english
 * @brief Log the message.
 *
 * @param level The log level to set.
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::log(const LogLevel level, const string msg, const Location location) {

    if (this->level <= level) {

        write(level, msg, location);
    }
}


/**
 * @~english
 * @brief Log an emergency message.
 *
 * @param msg The log message to write.
 */
void Logger::emerg(const string msg) {

    log(EMERG, msg);
}


/**
 * @~english
 * @brief Log an emergency message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::emerg(const string msg, const Location location) {

    log(EMERG, msg, location);
}


/**
 * @~english
 * @brief Log an alert message.
 *
 * @param msg The log message to write.
 */
void Logger::alert(const string msg) {

    log(ALERT, msg);
}


/**
 * @~english
 * @brief Log an alert message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::alert(const string msg, const Location location) {

    log(ALERT, msg, location);
}


/**
 * @~english
 * @brief Log a critical message.
 *
 * @param msg The log message to write.
 */
void Logger::crit(const string msg) {

    log(CRIT, msg);
}


/**
 * @~english
 * @brief Log a critical message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::crit(const string msg, const Location location) {

    log(CRIT, msg, location);
}


/**
 * @~english
 * @brief Log an error message.
 *
 * @param msg The log message to write.
 */
void Logger::err(const string msg) {

    log(ERR, msg);
}


/**
 * @~english
 * @brief Log an error message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::err(const string msg, const Location location) {

    log(ERR, msg, location);
}


/**
 * @~english
 * @brief Log a warning message.
 *
 * @param msg The log message to write.
 */
void Logger::warning(const string msg) {

    log(WARNING, msg);
}


/**
 * @~english
 * @brief Log a warning message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::warning(const string msg, const Location location) {

    log(WARNING, msg, location);
}


/**
 * @~english
 * @brief Log a notice message.
 *
 * @param msg The log message to write.
 */
void Logger::notice(const string msg) {

    log(NOTICE, msg);
}


/**
 * @~english
 * @brief Log a notice message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::notice(const string msg, const Location location) {

    log(NOTICE, msg, location);
}


/**
 * @~english
 * @brief Log an info message.
 *
 * @param msg The log message to write.
 */
void Logger::info(const string msg) {

    log(INFO, msg);
}


/**
 * @~english
 * @brief Log an info message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::info(const string msg, const Location location) {

    log(INFO, msg, location);
}


/**
 * @~english
 * @brief Log a debug message.
 *
 * @param msg The log message to write.
 */
void Logger::debug(const string msg) {

    log(DEBUG, msg);
}


/**
 * @~english
 * @brief Log a debug message.
 *
 * @param msg The log message to write.
 * @param location The message location.
 */
void Logger::debug(const string msg, const Location location) {

    log(DEBUG, msg, location);
}


/**
 * @~english
 * @brief Return the log level.
 *
 * @return The log level.
 */
LogLevel Logger::getLevel(void) {

    return this->level;
}


/**
 * @~english
 * @brief Set the log level.
 *
 * @param level The log level to set.
 */
void Logger::setLevel(const LogLevel level) {
    
    this->level = level;
}


/**
 * @~english
 * @brief Return the writer.
 *
 * @return The writer.
 */
Writer* Logger::getWriter(void) {

    return this->writer;
}


/**
 * @~english
 * @brief Return the writer for this given name. It is used to get
 * directly a writer from a multi writer. Note that this writer
 * is returned if we do not have a multi writer.
 * @param name The name of a writer that is registered to a multi writer.
 * @return The writer.
 */
Writer* Logger::getWriter(const string name) {

    if (dynamic_cast<MultiWriter *>(this->writer) == 0) {
        
        return this->writer;
    }
    
    return ((MultiWriter *)this->writer)->getWriter(name);
}


/**
 * @~english
 * @brief Set the writer.
 *
 * @param The writer to set.
 */
void Logger::setWriter(Writer *writer) {

    this->writer = writer;
}


/**
 * @~english
 * @brief Add a writer to a multi writer. It does not add anything
 * if the writer is not a multi writer.
 *
 * @param name The name of the writer to add.
 * @param writer The writer to add.
 */
void Logger::addWriter(const string name, Writer *writer) {
    
    if (dynamic_cast<MultiWriter *>(this->writer) == 0) {

        cout << "ERROR : Could not add a writer to a non multi writer" << endl;
        return;
    }

    ((MultiWriter *)this->writer)->addWriter(name, writer);
}


/**
 * @~english
 * @brief Remove a writer from a multi writer. It does not remove anything
 * if the writer is not a multi writer.
 *
 * @param name The name of the writer to add.
 */
void Logger::removeWriter(const string name) {
    
    if (dynamic_cast<MultiWriter *>(this->writer) == 0) {

        cout << "ERROR : Could not remove a writer from a non multi writer" << endl;
        return;
    }

    ((MultiWriter *)this->writer)->removeWriter(name);
}

/* -------------------------------------------------------------------------- */
