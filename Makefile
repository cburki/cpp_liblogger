## 
## Filename     : Makefile
## Description  : Makefile for the logger library.
## Author       : Christophe Burki
## Version      : 1.0.0
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License version 3 as
## published by the Free Software Foundation.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file LICENSE.  If not, write to the
## Free Software Foundation, Inc., 51 Franklin Street, Fifth
## ;; Floor, Boston, MA 02110-1301, USA.
## 
######################################################################


PLATFORM := /opt/platform.mk
ifeq ($(wildcard $(PLATFORM)),$(PLATFORM))
    include /opt/platform.mk
endif

#
# --------------------------------------------------------------------
#

ifndef PREFIX
	PREFIX := /usr/local
endif
CINC := -I$(PREFIX)/include
CLIB := -L$(PREFIX)/lib

CPPFLAGS := -Wall -std=c++11
LDFLAGS := -std=c++11

CPPFLAGS += $(CINC)
LDFLAGS += $(CLIB) -lstdc++

TEST_SOURCES := $(wildcard *_test.cpp)
SOURCES := $(filter-out $(TEST_SOURCES), $(wildcard *.cpp))
INCLUDES :=
TEST_OBJECTS := $(addsuffix .o, $(basename $(TEST_SOURCES)))
OBJECTS := $(addsuffix .o, $(basename $(SOURCES)))
STATIC := liblogger.a
SHARED := liblogger.so
TEST_TARGET := test


#
# --------------------------------------------------------------------
#

.PHONY : all clean distclean
.FORCE :

all : $(STATIC) $(SHARED)

$(STATIC) : $(OBJECTS)
	$(CXX) -c $(CPPFLAGS) $(SOURCES) $(LDFLAGS)
	$(AR) -cvq $(STATIC) $(OBJECTS)

$(SHARED) : $(OBJECTS)
	$(CXX) -c $(CPPFLAGS) -fpic $(SOURCES) $(LDFLAGS)
	$(CXX) -shared -Wl,-soname,$(SHARED).1 -o $(SHARED).1.0 $(OBJECTS)
	ln -sf $(SHARED).1.0 $(SHARED).1
	ln -sf $(SHARED).1.0 $(SHARED)

%.o : %.cpp
	$(CXX) -c -o $@ $< -fpic $(CPPFLAGS) $(INCLUDES)

logger_test.o : logger_test.cpp
	$(CXX) -c -o $@ $< $(CPPFLAGS) $(INCLUDES)

$(TEST_TARGET): $(TEST_OBJECTS) .FORCE
	$(CXX) $(TEST_OBJECTS) -o $@ $(LDFLAGS) -llogger
	./test

clean :
	rm -rf *.o
	rm -rf $(STATIC)
	rm -rf $(SHARED)*
	rm -rf $(TEST_TARGET)

distclean : clean
	rm -f *~
	rm -f *.log

install : $(STATIC) $(SHARED)
	mkdir -p $(PREFIX)/lib
	cp $(SHARED).1.0 $(PREFIX)/lib/$(SHARED).1.0
	mkdir -p $(PREFIX)/include/logger
	cp *.h $(PREFIX)/include/logger/.
	cd $(PREFIX)/lib && ln -sf $(SHARED).1.0 $(SHARED).1
	cd $(PREFIX)/lib && ln -sf $(SHARED).1.0 $(SHARED)
	ldconfig
