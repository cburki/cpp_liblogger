/* 
 * Filename     : log_level.h
 * Description  : Defines the log level.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_LOGLEVEL
#define BO_LOGLEVEL

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>

using std::string;
using std::map;

/* -------------------------------------------------------------------------- */

enum LogLevel {
    EMERG   = 7,
	ALERT   = 6,
	CRIT    = 5,
	ERR     = 4,
	WARNING = 3,
	NOTICE  = 2,
	INFO    = 1,
	DEBUG   = 0
};

const string logLevelString[] = {"DEBUG",
                                 "INFO",
                                 "NOTICE",
                                 "WARNING",
                                 "ERROR",
                                 "CRITICAL",
                                 "ALERT",
                                 "EMERGENCY"};

const map<string, LogLevel> string2LogLevel = { {"DEBUG"     , DEBUG},
											    {"INFO"      , INFO},
											    {"NOTICE"    , NOTICE},
											    {"WARNING"   , WARNING},
											    {"ERR"       , ERR},
											    {"ERROR"     , ERR},
											    {"CRIT"      , CRIT},
											    {"CRITICAL"  , CRIT},
											    {"ALERT"     , ALERT},
											    {"EMERG"     , EMERG},
											    {"EMERGENCY" , EMERG} };

/* -------------------------------------------------------------------------- */

#endif
