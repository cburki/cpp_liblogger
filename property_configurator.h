/* 
 * Filename     : property_configurator.h
 * Description  : Class for configuring the logger properties.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_PROPERTYCONFIGURATOR
#define BO_PROPERTYCONFIGURATOR

/* -------------------------------------------------------------------------- */

#include <string>
#include <vector>
#include <map>

#include "logger.h"

using std::string;
using std::vector;
using std::map;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Class for configuring the properties.
 */
class PropertyConfigurator {
    
 private :
    PropertyConfigurator(void);
    ~PropertyConfigurator(void);

    static map<string, string> *properties;
    
 public :
    static void configureFromMap(const map<string, string> properties);
    static void configureFromFile(const string filename);
    static map<string, string>* getProperties(void);
    static bool isConfigured(void);
    static vector<string> getRootWriters(void);
    static LogLevel getRootLevel(void);
    static LogLevel getLoggerLevel(const string name);
    static string getProperty(const string key, const string defValue = "");
    static void setProperty(const string key, const string value);
    static void clear(void);
    static unsigned int size(void);
    static string toString(void);
};

/* -------------------------------------------------------------------------- */

#endif
