/* 
 * Filename     : console_writer.h
 * Description  : Class for writing logs to the console.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_CONSOLEWRITER
#define BO_CONSOLEWRITER

/* -------------------------------------------------------------------------- */

#include <string>
#include "writer.h"

using std::string;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Writer class for writing to a console.
 */
class ConsoleWriter : public Writer {

 public :
    ConsoleWriter(void);
    ~ConsoleWriter(void);
    void write(const string msg);
    void write(const string msg, const string name, const LogLevel level, const string date, const Location location);
};

/* -------------------------------------------------------------------------- */

#endif
