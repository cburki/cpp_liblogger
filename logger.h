/* 
 * Filename     : logger.h
 * Description  : Class defining a logger.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_LOGGER
#define BO_LOGGER

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>

#include "level.h"
#include "location.h"
#include "writer.h"
#include "multi_writer.h"

using std::string;
using std::map;

/* -------------------------------------------------------------------------- */

#define LOG_LOCATION (Location(__FILE__, __func__, __LINE__))

#define LOG_EMERG(logger, msg) (logger)->emerg(msg, LOG_LOCATION)
#define LOG_ALERT(logger, msg) (logger)->alert(msg, LOG_LOCATION)
#define LOG_CRIT(logger, msg) (logger)->crit(msg, LOG_LOCATION)
#define LOG_ERR(logger, msg) (logger)->err(msg, LOG_LOCATION)
#define LOG_WARNING(logger, msg) (logger)->warning(msg, LOG_LOCATION)
#define LOG_NOTICE(logger, msg) (logger)->notice(msg, LOG_LOCATION)
#define LOG_INFO(logger, msg) (logger)->info(msg, LOG_LOCATION)
#define LOG_DEBUG(logger, msg) (logger)->debug(msg, LOG_LOCATION)

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Class for logging messages.
 */
class Logger {

 private :
    string name;
    LogLevel level;
    Writer *writer;

    void write(const LogLevel level, const string msg);
    void write(const LogLevel level, const string msg, const Location location);

 public :
    Logger(const LogLevel level, const string name, Writer *writer);
    
    void log(const LogLevel level, const string msg);
    void log(const LogLevel level, const string msg, const Location location);
    void emerg(const string msg);
    void emerg(const string msg, const Location location);
    void alert(const string msg);
    void alert(const string msg, const Location location);
    void crit(const string msg);
    void crit(const string msg, const Location location);
    void err(const string msg);
    void err(const string msg, const Location location);
    void warning(const string msg);
    void warning(const string msg, const Location location);
    void notice(const string msg);
    void notice(const string msg, const Location location);
    void info(const string msg);
    void info(const string msg, const Location location);
    void debug(const string msg);
    void debug(const string msg, const Location location);
    
    LogLevel getLevel(void);
    void setLevel(const LogLevel level);
    Writer* getWriter(void);
    Writer* getWriter(const string name);
    void setWriter(Writer *writer);
    void addWriter(const string name, Writer *writer);
    void removeWriter(const string name);
};

/* -------------------------------------------------------------------------- */

#endif
