// 
// Filename     : multi_writer.cpp
// Description  : Class that write logs to several writers simultaneously.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <iostream>
#include <map>

#include "multi_writer.h"
#include "level.h"
#include "location.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english Constructor
 * @brief
 */
MultiWriter::MultiWriter(void) {

    this->format = defaultFormat;
    this->datetimeFormat = defaultDatetimeFormat;
    this->writers = new map<string, Writer*>;
}


/**
 * @~english Constructor
 * @brief
 *
 * @param writers The list a writers to write the messages to.
 */
MultiWriter::MultiWriter(map<string, Writer *> *writers) {

    this->format = defaultFormat;
    this->datetimeFormat = defaultDatetimeFormat;
    this->writers = writers;
}


/**
 * @~english Destructor
 * @brief
 *
 */
MultiWriter::~MultiWriter(void) {
}


/**
 * @~english
 * @brief Write the log message to all the writers.
 *
 * @param msg The messate to write to the writers.
 */
void MultiWriter::write(const string msg) {

    for (map<string, Writer *>::iterator i = writers->begin(); i != writers->end(); i++) {

        Writer *writer = i->second;
        writer->write(msg);
    }
}


/**
 * @~english
 * @brief Writer the log message to all writers.
 *
 * @param msg The message to write to the console.
 * @param name The name of the logger.
 * @param level The log level.
 * @param date The date.
 * @param location The message location.
 */
void MultiWriter::write(const string msg, const string name, const LogLevel level, const string date, const Location location) {

    for (map<string, Writer *>::iterator i = writers->begin(); i != writers->end(); i++) {

        Writer *writer = i->second;
        writer->write(msg, name, level, date, location);
    }
}


/**
 * @~english
 * @brief Add a writer.
 *
 * @param name The name of the writer to add.
 * @param writer The writer to add.
 */
void MultiWriter::addWriter(const string name, Writer *writer) {
    
    if (writers->find(name) != writers->cend()) {
        
        cout << "ERROR : A writer already exist with the name \"" + name + "\"" << endl;
        return;
    }
    
    writers->insert(make_pair(name, writer));
}


/**
 * @~english
 * @brief Remove a writer.
 *
 * @param name The name of the writer to remove.
 */
void MultiWriter::removeWriter(const string name) {

    if (writers->find(name) == writers->cend()) {
        
        cout << "WARNING : No writer exist with the name \"" + name + "\"" << endl;
        return;
    }

    writers->erase(name);
}


/**
 * @~english
 * @brief Return the writer with the given name.
 *
 * @param name The name of the writer to remove.
 * @return The found writer or NULL if no writer found with that name.
 */
Writer* MultiWriter::getWriter(const string name) {
    
    if (writers->find(name) == writers->cend()) {
        
        cout << "WARNING : No writer exist with the name \"" + name + "\"" << endl;
        return NULL;
    }

    return writers->find(name)->second;
}


/**
 * @~english
 * @brief Return the number of writers.brief
 *
 * @return The number of writers.
 */
unsigned int MultiWriter::size(void) {

    return writers->size();
}

/* -------------------------------------------------------------------------- */
