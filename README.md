C++ Logging Facility
=======================


Summary
-------

Logging facility library that allow to output log messages into writers. The
following writers are currently supported.

 - Console : Write messages on the console.
 - File    : Write messages into a file.
 - String  : Write messages in a string.
 - Multi   : Write messages simultaneously to several writers.
 
The library supports levels which determines the importance of the message. Higher
is the level number most important is the message. Here are the levels and their
values.

 - EMERGENCY : 7; System is unusable
 - ALERT     : 6; Action must be taken immediately
 - CRITICAL  : 5; Critical conditions
 - ERROR     : 4; Error conditions
 - WARNING   : 3; Warning conditions
 - NOTICE    : 2; Normal, but significant, condition
 - INFO      : 1; Informational message
 - DEBUG     : 0; Debug message

The minimum logging level is given during the creation of a logger or it can
be setted afterward by calling the SetLevel function. Only messages with a
higher level that the configured one are displayed.

Logger could be created in two different ways. The first way is to create
the writer and then instanciate a logger giving the log level, the name of
the logger and the writer. The second way is by configuring the logging
system and let the factory instanciate the loggers from the configured
properties.


Direct Instanciation
--------------------

The direct instanciation is used when you need a simple logger. Here is a
sample on how to write log messages on the console writer.

    #include "logger/logger.h"
    #include "logger/console_writer.h"
    
    ConsoleWriter writer;
    Logger logger(INFO, "TestConsoleWriter", &writer);
    string s = "EMERGENCY";
    logger.emerg("This is an " + s + " message");
    logger.info("This is an INFO message");
    logger.debug("This is a DEBUG message");

In the example above, the DEBUG message is not displayed because we have set
the logger level to INFO which is higher that the DEBUG level.

More example could be found in the *logger_test.cpp* file.


Logging Configuration
---------------------

A better way of instantiating loggers is from the factory using a configuration.
In this way, subsystem could have its own logger each with a different level.
The logger can be configured with properties coming from a file or from a map.
Here is a property file example.

    logger.rootLogger.level        = INFO
    logger.rootLogger.writers      = CONSOLE, FILE
    logger.writer.CONSOLE          = ConsoleWriter
    logger.writer.CONSOLE.format   = %d %l [%n] %f/%c %b : %m
    logger.writer.CONSOLE.dtformat = %Y-%m-%d %H:%M:%S
    logger.writer.FILE             = FileWriter
    logger.writer.FILE.filename    = test_factory.log
    logger.writer.FILE.append      = false
    logger.level.Test_1            = DEBUG
    logger.level.Test_2            = WARNING

First of all, we must set a root logger level (*logger.rootLogger.level*) which
is used to defined the log level when no other specific levels are defined. Then
we declare the writers we want to use (*logger.rootLogger.writers*). Several
writers can be used and they are specified by a comma separated list. For each
writers we have defined in this list, we need to specify the module and the 
class name to used for instanciating them. See the *logger.writer.CONSOLE* and
*logger.writer.FILE* properties. Writers can have options as you can see for the
*CONSOLE* writer (format and dtformat option) and *FILE* writer (filename and
append options). The log level for each components is then configured. Here 
you can see that the *TestLogger* has the level *INFO* and the *AnotherTestLogger* 
has the level *DEBUG*.

The format of a line of log could be configured thanks to the *format* option
of writers. So each writers could have a different format. Here the supported
replacement formats.

 - %d : The date and time.
 - %l : The log level.
 - %n : The logger name.
 - %m : The log message.
 - %f : The filename from where the log is written.
 - %c : The function name from where the log is written.
 - %b : The line number from where the log is written.

The default format is "%d %l [%n] : %m".
    
The date and time format could also be configured with the *dtformat* option. The
default format is "%Y-%m-%d %H:%M:%S". You can refer to the strftime function
for the supported format.

The logger can also be configured with properties coming from a map. Here is an
example of declaring a property map.

    std::map<string, string> props = {
        {"logger.rootLogger.level"        , "INFO"},
        {"logger.rootLogger.writers"      , "CONSOLE, FILE"},
        {"logger.writer.CONSOLE"          , "ConsoleWriter"},
        {"logger.writer.CONSOLE.format"   , "%d %l [%n] %f/%c %b : %m"},
        {"logger.writer.CONSOLE.dtformat" , "%Y-%m-%d %H:%M:%S"},
        {"logger.writer.FILE"             , "FileWriter"},
        {"logger.writer.FILE.filename"    , "test.log"},
        {"logger.writer.FILE.append"      , "true"},
        {"logger.level.TestLogger"        , "INFO"},
        {"logger.level.AnotherTestLogger" , "DEBUG"}
    };

When using this logging facilities with the configuration system, you must use
these log macro below for logging your messages.

 - LOG_EMERG(logger, msg)
 - LOG_ALERT(logger, msg)
 - LOG_CRIT(logger, msg)
 - LOG_ERR(logger, msg)
 - LOG_WARNING(logger, msg)
 - LOG_NOTICE(logger, msg)
 - LOG_INFO(logger, msg)
 - LOG_DEBUG(logger, msg)

Here below is a sample showing how to use the configurator with the file here
above and how to instantiate the loggers for components. The commented out line
show you how to configure teh system from a map.

    #include "logger/logger.h"
    #include "logger/property_configurator.h"
    #include "logger/logger_factory.h"
    
    PropertyConfigurator::configureFromFile("property_file.ini");
    // PropertyConfigurator::configureFromMap(props);
    Logger *testLogger = LoggerFactory::getLogger("TestLogger);
    Logger *anotherTestLogger = LoggerFactory::getLogger("AnotherTestLogger");
    Logger *oneAnotherLogger = LoggerFactory::getLogger("OneAnotherLogger");
    
    LOG_ERR(testLogger, "This is an error message");
    
According to the configuration here before, the *oneAnotherLogger* has no specific
log level configured so it will have the default log level defined by the 
*logger.rootLogger.level" property.

Loggers properties could be changed from code after configuring them. The sample
below show how the set the level and the log format for the *testLogger*.

    testLogger->setLevel(WARNING);
    testLogger->getWriter("CONSOLE")->setFormat("%d %l [%n] : %m");

Instead of using properties to configure the logging system, you could also
set it up manually directly in the code. Here is an example.

    #include "logger/logger.h"
    #include "logger/logger_factory.h"
    #include "logger/console_writer.h"
    #include "logger/file_writer.h"
    
    Logger *logger = LoggerFactory::getLogger("Test");
    ConsoleWriter cWriter;
    FileWriter fWriter("test_setup.log", true);
    logger->addWriter("CONSOLE", &cWriter);
    logger->addWriter("FILE", &fWriter);
    logger->setLevel(NOTICE);
    logger->getWriter("CONSOLE")->setFormat("%d %l [%n] %f/%c (%b) : %m");

In the example above, the logger must be configured by adding writers and
eventually setting the level and format. The logger will be useless if you
just get it from the factory.
