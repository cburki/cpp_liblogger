// 
// Filename     : ini_parser.cpp
// Description  : Class for parsing an INI file.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "ini_parser.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Trim whitespaces on the right of the string.
 */
inline string trim_right_inplace(string s, const string delimiters) {
    return s.erase(s.find_last_not_of(delimiters) + 1);
}

/**
 * @~english
 * @brief Trime whitspaces on the left of the string.
 */
inline string trim_left_inplace(string s, const string delimiters) {
    return s.erase(0, s.find_first_not_of(delimiters));
}

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 */
IniParser::IniParser(void) {

    this->properties = {};
}


/**
 * @~english
 * @brief Reutrn the properties map.
 *
 * @return The properties map.
 */
map<string, string> IniParser::getProperties(void) {

    return properties;
}


/**
 * @~english
 * @brief Parse the file and store the found key/value pairs.
 *
 * @param filename The name of the INI file to parse.
 */
void IniParser::parseFile(const string filename) {
    
    ifstream input;
    input.open(filename, ios_base::in);
    
    if (! input.is_open()) {
        
        cout << "Error : Could not open file \"" << filename << "\"" << endl;
        return;
    }

    string stream((istreambuf_iterator<char>(input)), istreambuf_iterator<char>());
    parseStream(stream);
    
    input.close();
}


/**
 * @~english
 * @brief Parse a stream and store the found key/value pairs.
 *
 * @param stream The stream to parse.
 */
void IniParser::parseStream(const string stream) {

    stringstream ss(stream);
    string line;
    
    while (getline(ss, line)) {

        /* Skip empty lines. */
        if (line.length() == 0) {

            continue;
        }
        
        /* Skip commented out lines. */
        unsigned int foundSemicolon = line.find(";");
        if ((foundSemicolon != string::npos) && (foundSemicolon == 0)) {

            continue;
        }

        /* Skip commented out lines. */
        unsigned int foundHash = line.find("#");
        if ((foundHash != string::npos) && (foundHash == 0)) {

            continue;
        }
        
        /* Skip line not having =. */
        if (line.find("=") == string::npos) {

            cout << "WARNING : Skip line \"" << line << "\", does not seems to have key/value pair" << endl;
            continue;
        }
        
        /* Extract the key/value pair. */
        stringstream lss(line);
        string key;
        string value;
        getline(lss, key, '=');
        getline(lss, value);

        /* Store the key/value pair. */
        properties.insert(make_pair(trim(key), trim(value)));
    }

}


/**
 * @~english
 * @brief Return the value for the given key.
 *
 * @param key The key for which to return the value.
 * @return The value or an empty string when the key is not found.
 */
string IniParser::get(const string key) {

    map<string, string>::const_iterator itr = properties.find(key);
    
    if (itr == properties.cend()) {
        
        return "";
    }
    
    return itr->second;
}


/**
 * @~english
 * @brief Remove whitespaces in the beginning and end of string.
 *
 * @param s The string from which to trim whitspaces.
 * @return The trimmed string.
 */
string IniParser::trim(const string s) {
    
    const string delimiters = " \t";
    return trim_left_inplace(trim_right_inplace(s, delimiters), delimiters);
}

/* -------------------------------------------------------------------------- */
