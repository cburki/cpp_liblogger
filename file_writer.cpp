// 
// Filename     : file_writer.cpp
// Description  : Class for writing logs into a file.
// Maintainer   : Christophe Burki
// Version      : 1.0.0
// 
// 
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 
// 
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <iostream>
#include <fstream>

#include "file_writer.h"
#include "level.h"
#include "location.h"
#include "formatter.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english Constructor
 * @brief
 *
 * @param filename The name of the file where to write the messages.
 * @param append Wheather to append to the file or not.
 */
FileWriter::FileWriter(const string filename, const bool append) {

    this->format = defaultFormat;
    this->datetimeFormat = defaultDatetimeFormat;
    
    ios_base::openmode mode = ios_base::out|ios_base::trunc;
    if (append) {
        mode = ios_base::out|ios_base::app;
    }
    
    logFile.open(filename, mode);
    if (!logFile.is_open()) {
        cout << "Could not open log file \"" << filename << "\" !" << endl;
    }
}


/**
 * @~english Destructor
 * @brief
 */
FileWriter::~FileWriter(void) {

    logFile.close();
}


/**
 * @~english
 * @brief Write the log message into a file.
 *
 * @param msg The message to write into the file.
 */
void FileWriter::write(const string msg) {

    logFile << msg << endl;
}


/**
 * @~english
 * @brief Writer the log message into a file.
 *
 * @param msg The message to write to the console.
 * @param name The name of the logger.
 * @param level The log level.
 * @param date The date.
 * @param location The message location.
 */
void FileWriter::write(const string msg, const string name, const LogLevel level, const string date, const Location location) {

    Formatter formatter(format);
    write(formatter.format(msg, name, level, date, location));
}

/* -------------------------------------------------------------------------- */
