/* 
 * Filename     : init_parser.h
 * Description  : Class for parsing an INI file.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_INIPARSER
#define BO_INIPARSER

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>

using std::string;
using std::map;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Class for parsing a INI file and bulding.
 */
class IniParser {

 private :
    map<string, string> properties;
    
    string trim(const string s);

 public :
    IniParser(void);
    map<string, string> getProperties(void);
    void parseFile(const string filename);
    void parseStream(const string stream);
    string get(const string key);
};

/* -------------------------------------------------------------------------- */

#endif
