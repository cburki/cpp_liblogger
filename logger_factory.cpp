// 
// Filename     : logger_factory.cpp
// Description  : Class defining the logger factory.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include "property_configurator.h"
#include "writer.h"
#include "logger_factory.h"
#include "console_writer.h"
#include "file_writer.h"
#include "multi_writer.h"

using namespace std;

/* -------------------------------------------------------------------------- */

map<string, Logger*> LoggerFactory::loggers;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 */
LoggerFactory::LoggerFactory(void) {
}


/**
 * @~english
 * @brief Destructor
 */
LoggerFactory::~LoggerFactory(void) {
}


/**
 * @~english
 * @brief Return a new logger for the given name and related configured properties.brief
 *
 * @param name The name of the logger to get.
 * @return The new logger or NULL on error.
 */
Logger* LoggerFactory::getLogger(const string name) {

    /* Check if the logger has been configured. */
    if (!PropertyConfigurator::isConfigured()) {
        
        cout << "ERROR : Logger is not configured." << endl;
        return NULL;
    }
    
    /* Return the logger if it has been already created. */
    if (loggers.find(name) != loggers.cend()) {
        
        return loggers[name];
    }

    /* Find the logger level. */
    LogLevel loggerLogLevel = PropertyConfigurator::getLoggerLevel(name);
    
    /* Find all writers from property logger.rootLogger.writers and instantiate them. */
    map<string, Writer *> *writers = new map<string, Writer *>;
    vector<string> rootWriters = PropertyConfigurator::getRootWriters();
    
    for (vector<string>::const_iterator i = rootWriters.cbegin(); i != rootWriters.cend(); i++) {
        
        string writerName = *i;
        
        map<string, string> *props = PropertyConfigurator::getProperties();
        if (props->find("logger.writer." + writerName) == props->cend()) {
            
            cout << "ERROR : No writer found for \"" << writerName << "\"" << endl;
            delete writers;
            return NULL;
        }

        /* Retrieve the format and datetime format. */
        string format = PropertyConfigurator::getProperty("logger.writer." + writerName + ".format", defaultFormat);
        string datetimeFormat = PropertyConfigurator::getProperty("logger.writer." + writerName + ".dtformat", defaultDatetimeFormat);

        /* Instantiate the writers. */
        string writerClassName = PropertyConfigurator::getProperty("logger.writer." + writerName);
        if (writerClassName.compare("ConsoleWriter") == 0) {

            ConsoleWriter *consoleWriter = new ConsoleWriter();
            consoleWriter->setFormat(format);
            consoleWriter->setDatetimeFormat(datetimeFormat);
            writers->insert(make_pair(writerName, consoleWriter));
        }
        else if (writerClassName.compare("FileWriter") == 0) {

            /* Get the filename from the properties. */
            string filename = PropertyConfigurator::getProperty("logger.writer." + writerName + ".filename");
            if (filename.length() == 0) {
                
                cout << "ERROR : Could not find the filename property for writer \"" << writerName << "\"" << endl;
                delete writers;
                return NULL;
            }
            
            /* Get the append flag from the properties. */
            bool append = true;
            string app = PropertyConfigurator::getProperty("logger.writer." + writerName + ".append", "true");
            if (app.compare("true") != 0) {
                
                append = false;
            }
            
            FileWriter *fileWriter = new FileWriter(filename, append);
            fileWriter->setFormat(format);
            fileWriter->setDatetimeFormat(datetimeFormat);
            writers->insert(make_pair(writerName, fileWriter));
        }
        else if (writerClassName.compare("StringWriter") == 0) {
            
            cout << "ERROR : StringWriter could not be instantiated from the logger factory." << endl;
            delete writers;
            return NULL;
        }
        else if (writerClassName.compare("MultiWriter") == 0) {
            
            cout << "ERROR : MultiWriter could not be instantiated from the logger factory." << endl;
            delete writers;
            return NULL;
        }
        else {
            
            cout << "ERROR : Unknown writer \"" << writerClassName << "\"" << endl;
            delete writers;
            return NULL;
        }
    }

    if (writers->size() == 0) {
        
    }

    MultiWriter *writer = new MultiWriter(writers);
    Logger *logger = new Logger(loggerLogLevel, name, writer);
    loggers.insert(make_pair(name, logger));
    return logger;
}

/* -------------------------------------------------------------------------- */
