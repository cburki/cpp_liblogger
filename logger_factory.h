/* 
 * Filename     : logger_factory.h
 * Description  : Class defining the logger factory.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_LOGGERFACTORY
#define BO_LOGGERFACTORY

/* -------------------------------------------------------------------------- */

#include <string>
#include <map>
#include "logger.h"

using std::string;
using std::map;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Class for instanciating loggers according to configuration properties.
 */
class LoggerFactory {
    
 private :
    LoggerFactory(void);
    ~LoggerFactory(void);
    
    static map<string, Logger*> loggers;
  
 public :
    static Logger* getLogger(const string name);
};

/* -------------------------------------------------------------------------- */

#endif
