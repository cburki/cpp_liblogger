/* 
 * Filename     : multi_writer.h
 * Description  : Class that write logs to several writers simultaneously.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_MULTIWRITER
#define BO_MULTIWRITER

/* -------------------------------------------------------------------------- */

#include <map>
#include "writer.h"

using std::string;
using std::map;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Writer class for writing into several writers.
 */
class MultiWriter : public Writer {
    
 private :
    map<string, Writer *> *writers;
    
 public :
    MultiWriter(void);
    MultiWriter(map<string, Writer *> *writers);
    ~MultiWriter(void);
    void write(const string msg);
    void write(const string msg, const string name, const LogLevel level, const string date, const Location location);
    void addWriter(const string name, Writer *writer);
    void removeWriter(const string name);
    Writer* getWriter(const string name);
    unsigned int size(void);
};

/* -------------------------------------------------------------------------- */

#endif
