/* 
 * Filename     : location.h
 * Description  : Class defining the log location.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_LOCATION
#define BO_LOCATION

/* -------------------------------------------------------------------------- */

#include <string>

using std::string;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief
 */
class Location {

 private :
    string filename;
    string funcname;
    unsigned long line;
    
 public :
    Location(void);
    Location(const string filename, const string funcname, const unsigned long line);
    ~Location(void);
    string getFilename(void) const;
    string getFuncname(void) const;
    unsigned long getLine(void) const;
};

/* -------------------------------------------------------------------------- */

#endif
