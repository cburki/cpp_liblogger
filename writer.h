/* 
 * Filename     : writer.h
 * Description  : Abstract class for writing logs.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_WRITER
#define BO_WRITER

/* -------------------------------------------------------------------------- */

#include <string>
#include "location.h"
#include "level.h"

using std::string;

/* -------------------------------------------------------------------------- */

const string defaultFormat = "%d %l [%n] : %m";
const string defaultDatetimeFormat = "%Y-%m-%d %H:%M:%S";

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Abstract class defining a writer.
 */
class Writer {

 protected :
    string format;
    string datetimeFormat;
    
 public :
    virtual void write(const string msg) = 0;
    virtual void write(const string msg, const string name, const LogLevel level, const string date, const Location location) = 0;
    void setFormat(const string format);
    string getFormat(void);
    void setDatetimeFormat(const string format);
    string getDatetime(void);
};

/* -------------------------------------------------------------------------- */

#endif
