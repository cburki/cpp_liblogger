// 
// Filename     : property_configurator.cpp
// Description  : Class for configuring the logger properties.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>

#include "property_configurator.h"
#include "ini_parser.h"

using namespace std;

/* -------------------------------------------------------------------------- */

map<string, string> *PropertyConfigurator::properties = NULL;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 */
PropertyConfigurator::PropertyConfigurator(void) {
}


/**
 * @~english
 * @brief Destructor
 */
PropertyConfigurator::~PropertyConfigurator(void) {
}


/**
 * @~english
 * @brief Configure the logging system with the given map of properties.
 *
 * @param properties The properties to configure with.
 */
void PropertyConfigurator::configureFromMap(const map<string, string> properties) {

    PropertyConfigurator::properties = new map<string, string>;

    /* Checks for required properties (logger.rootLogger.level, logger.rootLogger.writers) */
    if ((properties.find("logger.rootLogger.level") == properties.cend()) || (properties.find("logger.rootLogger.writers") == properties.cend())) {
        
        cout << "ERROR : Missing required properties logger.rootLogger.level or logger.rootLogger.writers" << endl;
        return;
    }
    
    PropertyConfigurator::properties->insert(properties.cbegin(), properties.cend());
}


/**
 * @~english
 * @brief
 *
 * @param filename The file from which to read the properties.
 */
void PropertyConfigurator::configureFromFile(const string filename) {
    
    IniParser parser;
    parser.parseFile(filename);
    PropertyConfigurator::configureFromMap(parser.getProperties());
}


/**
 * @~english
 * @brief Return the map of configuration properties.
 *
 * @return The map of properties.
 */
map<string, string>* PropertyConfigurator::getProperties(void) {
    
    return PropertyConfigurator::properties;
}


/**
 * @~english
 * @brief Check wheather the logginf system is configured or not.brief
 *
 * @return Wheater the logging system is configured or not.
 */
bool PropertyConfigurator::isConfigured(void) {
    
    return (PropertyConfigurator::properties != NULL);
}


/**
 * @~english
 * @brief Return the configured root writers from the configuration option
 * logger.rootLogger.writers.
 *
 * @return A list of writers name or an empty list of none are found.
 */
vector<string> PropertyConfigurator::getRootWriters(void) {

    vector<string> writers;
    if (PropertyConfigurator::properties->find("logger.rootLogger.writers") == PropertyConfigurator::properties->cend()) {
        
        return writers;
    }
    
    stringstream ss(PropertyConfigurator::properties->find("logger.rootLogger.writers")->second);
    while (ss.good()) {
        
        string token = "";
        getline(ss, token, ',');
        string::iterator end = remove(token.begin(), token.end(), ' ');
        token.erase(end, token.end());
        writers.push_back(token);
    }
    
    return writers;
}


/**
 * @~english
 * @brief Return the default configured log level. Return INFO if
 * no default log level is configured.
 *
 * @return The default log level.
 */
LogLevel PropertyConfigurator::getRootLevel(void) {
    
    if (PropertyConfigurator::properties->find("logger.rootLogger.level") == PropertyConfigurator::properties->cend()) {
        
        return INFO;
    }

    string level = PropertyConfigurator::properties->find("logger.rootLogger.level")->second;
    return string2LogLevel.find(level)->second;
}


/**
 * @~english
 * @brief Find the level configured for the given logger name. Return the
 * default log level if no log level is configured for the logger name.
 *
 * @param name The name of the logger to return the level for.
 * @return The log level for the logger.
 */
LogLevel PropertyConfigurator::getLoggerLevel(const string name) {
    
    if (PropertyConfigurator::properties->find("logger.level." + name) == PropertyConfigurator::properties->cend()) {
        
        return getRootLevel();
    }
    
    string level = PropertyConfigurator::properties->find("logger.level." + name)->second;
    return string2LogLevel.find(level)->second;
}


/**
 * @~english
 * @brief Find a property a return it. Return the given default property 
 * when not found.
 *
 * @param key The key for which to find the property.
 * @param defValue The default property to return when not found.
 * @return The founf or default property.
 */
string PropertyConfigurator::getProperty(const string key, const string defValue) {
    
    if (PropertyConfigurator::properties->find(key) == PropertyConfigurator::properties->cend()) {
        
        return defValue;
    }

    return PropertyConfigurator::properties->find(key)->second;
}


/**
 * @~english
 * @brief Set the given property value.
 *
 * @param key The key for which to set the property.
 * @param value The value to set for the property.
 */
void PropertyConfigurator::setProperty(const string key, const string value) {
    
    if (!PropertyConfigurator::isConfigured()) {
   
        PropertyConfigurator::properties = new map<string, string>;
    }
    
    PropertyConfigurator::properties->insert(make_pair(key, value));
}

/**
 * @~english
 * @brief Removes all properties.
 */
void PropertyConfigurator::clear(void) {
    
    PropertyConfigurator::properties->clear();
}


/**
 * @~english
 * @brief Return the number of properties.
 *
 * @return The numner of properties.
 */
unsigned int PropertyConfigurator::size(void) {
    
    return PropertyConfigurator::properties->size();
}


/**
 * @~english
 * @brief Return the string representation of the properties.
 *
 * @return The string representation.
 */
string PropertyConfigurator::toString(void) {
    
    if (!PropertyConfigurator::isConfigured()) {
        
        return "";
    }
    
    string out = "{ ";
    for (map<string, string>::const_iterator i = PropertyConfigurator::properties->cbegin(); i != PropertyConfigurator::properties->cend(); i++) {

        out += "\"" + i->first + "\" : \"" + i->second + "\"";
        map<string, string>::const_iterator itr = PropertyConfigurator::properties->cend();
        if (--itr != i) {
            out += ", ";
        }
    }
    out += " }";
    
    return out;
}

/* -------------------------------------------------------------------------- */
