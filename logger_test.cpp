// 
// Filename     : logger_test.cpp
// Description  : Test program for the logger library.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <array>

#include "logger.h"
#include "level.h"
#include "console_writer.h"
#include "file_writer.h"
#include "multi_writer.h"
#include "string_writer.h"
#include "ini_parser.h"
#include "property_configurator.h"
#include "logger_factory.h"
#include "location.h"
#include "formatter.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Test logging messages to the console.
 */
int testConsoleWriter(void) {

    ConsoleWriter writer;
    Logger logger(NOTICE, "TestConsoleWriter", &writer);

    string emergency = "EMERGENCY";
    logger.emerg("This is an " + emergency + " message");
    logger.alert("This is an ALERT message");
    logger.crit("This is a CRITICAL message");
    logger.err("This is a ERROR message");
    logger.warning("This is a WARNING message");
    logger.notice("This is a NOTICE message");
    logger.info("This is an INFO message");
    logger.debug("This is a DEBUG message");
    
    return 0;
}


/**
 * @~english
 * @brief Test loggin messages into a file.
 */
int testFileWriter(void) {

    FileWriter writer("test.log", false);
    writer.setDatetimeFormat("");
    Logger logger(INFO, "TestFileWriter", &writer);

    string emergency = "EMERGENCY";
    logger.emerg("This is an " + emergency + " message");
    logger.alert("This is an ALERT message");
    logger.crit("This is a CRITICAL message");
    logger.err("This is a ERROR message");
    logger.warning("This is a WARNING message");
    logger.notice("This is a NOTICE message");
    logger.info("This is an INFO message");
    logger.debug("This is a DEBUG message");

    /* Check log file content. */
    ifstream logFile;
    array<string,7> wants = {"EMERGENCY [TestFileWriter] : This is an EMERGENCY message\n",
                             "ALERT [TestFileWriter] : This is an ALERT message\n",
                             "CRITICAL [TestFileWriter] : This is a CRITICAL message\n",
                             "ERROR [TestFileWriter] : This is an ERROR message\n",
                             "WARNING [TestFileWriter] : This is a WARNING message\n",
                             "NOTICE [TestFileWriter] : This is a NOTICE message\n",
                             "INFO [TestFileWriter] : This is an INFO message\n"};
    
    logFile.open("test.log", ios_base::in);
    if (!logFile.is_open()) {

        cout << "Error : Could not open log file" << endl;
        return -1;
    }

    unsigned int i = 0;
    while (logFile.good()) {

        string line;
        getline(logFile, line);
        if (line.length() == 0) {
            continue;
        }
        
        if (!wants[i].compare(line) != 0) {

            cout << "Error : want \"" << wants[i] << "\", got \"" << line << endl;
            logFile.close();
            return -1;
        }
        i++;
    }
    
    //logFile.close();
    return 0;
}


/**
 * @~english
 * @brief Test logging messages to the console and to a file.
 */
int testMultiWriter(void) {

    ConsoleWriter cWriter;
    FileWriter fWriter("test_multi.log");
    map<string, Writer *> writers = {{"CONSOLE", &cWriter}, {"FILE", &fWriter}};
    MultiWriter writer(&writers);
    Logger logger(INFO, "TestMultiWriter", &writer);
    
    string emergency = "EMERGENCY";
    logger.emerg("This is an " + emergency + " message");
    logger.alert("This is an ALERT message");
    logger.crit("This is a CRITICAL message");
    logger.err("This is a ERROR message");
    logger.warning("This is a WARNING message");
    logger.notice("This is a NOTICE message");
    logger.info("This is an INFO message");
    logger.debug("This is a DEBUG message");

    return 0;
}


/**
 * @~english
 * @brief Test logging message into a string. It does also test the datetime format setting.
 */
int testStringWriter(void) {

    string *logMsg = new string();
    StringWriter writer(logMsg);
    writer.setDatetimeFormat("%Y-%m-%d");
    Logger logger(DEBUG, "TestStringWriter", &writer);

    time_t now = time(NULL);
    struct tm *timeinfo;
    char timeBuffer[64];
    timeinfo = localtime(&now);
    strftime(timeBuffer, 64, "%Y-%m-%d", timeinfo);
    string datetime = string(timeBuffer);
    
    string emergency = "EMERGENCY";
    logger.emerg("This is an " + emergency + " message");
    string want = datetime + " EMERGENCY [TestStringWriter] : This is an EMERGENCY message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }
    
    logger.alert("This is an ALERT message");
    want = datetime + " ALERT [TestStringWriter] : This is an ALERT message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.crit("This is a CRITICAL message");
    want = datetime + " CRITICAL [TestStringWriter] : This is a CRITICAL message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.err("This is an ERROR message");
    want = datetime + " ERROR [TestStringWriter] : This is an ERROR message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.warning("This is a WARNING message");
    want = datetime + " WARNING [TestStringWriter] : This is a WARNING message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.notice("This is a NOTICE message");
    want = datetime + " NOTICE [TestStringWriter] : This is a NOTICE message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.info("This is an INFO message");
    want = datetime + " INFO [TestStringWriter] : This is an INFO message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    logger.debug("This is a DEBUG message");
    want = datetime + " DEBUG [TestStringWriter] : This is a DEBUG message";
    if (want.compare(*logMsg) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << *logMsg << endl;
        delete logMsg;
        return -1;
    }

    delete logMsg;
    return 0;
}


/**
 * @~english
 * @brief Test the INI file parser.
 */
int testIniParser(void) {

    /* Test parsing from string. */
    string input = "; comment\n"
        "bad line\n"
        "logger.rootLogger.level = INFO\n"
        "logger.rootLogger.writers = CONSOLE\n";
    IniParser sParser;
    sParser.parseStream(input);
    string gotLevel = sParser.get("logger.rootLogger.level");
    if (gotLevel.compare("INFO") != 0) {
        
        cout << "Error : want \"INFO\", got \"" << gotLevel << "\"" << endl;
        return -1;
    }
    string gotWriters = sParser.get("logger.rootLogger.writers");
    if (gotWriters.compare("CONSOLE") != 0) {
        
        cout << "Error : want \"CONSOLE\", got \"" << gotWriters << "\"" << endl;
        return -1;
    }

    /* Test parsing from file. */
    IniParser fParser;
    fParser.parseFile("test.properties");
    gotLevel = fParser.get("logger.rootLogger.level");
    if (gotLevel.compare("INFO") != 0) {
        
        cout << "Error : want \"INFO\", got \"" << gotLevel << "\"" << endl;
        return -1;
    }
    gotWriters = fParser.get("logger.rootLogger.writers");
    if (gotWriters.compare("CONSOLE, FILE") != 0) {
        
        cout << "Error : want \"CONSOLE, FILE\", got \"" << gotWriters << "\"" << endl;
        return -1;
    }
    
    return 0;
}


/**
 * @~english
 * @brief Test the configuration of properties.
 */
int testPropertyConfigurator(void) {
 
    /* Test missing required properties. */
    map<string, string> propsMissing = { {"logger.writer.CONSOLE", "ConsoleWriter"} };
    PropertyConfigurator::configureFromMap(propsMissing);
    if (PropertyConfigurator::size() != 0) {
        
        cout << "Error : properties must be empty" << endl;
        return -1;
    }
    
    /* Test property configuration from map. */
    string want = "{ \"logger.level.Test_1\" : \"DEBUG\", \"logger.level.Test_2\" : \"WARNING\", \"logger.rootLogger.level\" : \"INFO\", \"logger.rootLogger.writers\" : \"CONSOLE, FILE\", \"logger.writer.CONSOLE\" : \"ConsoleWriter\", \"logger.writer.CONSOLE.format\" : \"%d %l [%n] %f/%c %b : %m\", \"logger.writer.FILE\" : \"FileWriter\", \"logger.writer.FILE.append\" : \"false\", \"logger.writer.FILE.dtformat\" : \"%Y-%m-%d\", \"logger.writer.FILE.filename\" : \"test_factory.log\", \"logger.writer.FILE.format\" : \"%d %l [%n] : %m\" }";
    map<string, string> props = { {"logger.rootLogger.level", "INFO"},
                                  {"logger.rootLogger.writers", "CONSOLE, FILE"},
                                  {"logger.writer.CONSOLE", "ConsoleWriter"},
                                  {"logger.writer.CONSOLE.format", "%d %l [%n] %f/%c %b : %m"},
                                  {"logger.writer.FILE", "FileWriter"},
                                  {"logger.writer.FILE.filename", "test_factory.log"},
                                  {"logger.writer.FILE.append", "false"},
                                  {"logger.writer.FILE.format", "%d %l [%n] : %m"},
                                  {"logger.writer.FILE.dtformat", "%Y-%m-%d"},
                                  {"logger.level.Test_1", "DEBUG"},
                                  {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(props);
    string got = PropertyConfigurator::toString();
    if (want.compare(got) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << got << "\"" << endl;
        return -1;
    }
    
    /* Test property configuration from file. */
    PropertyConfigurator::clear();
    PropertyConfigurator::configureFromFile("test.properties");
    got = PropertyConfigurator::toString();
    if (want.compare(got) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << got << "\"" << endl;
        return -1;
    }
    
    return 0;
}


/**
 * @~english
 * @brief Test the instanciation of loggers from the factory.
 */
int testLoggerFactory(void) {

    /* Test no writers defined. */
    map<string, string> propsNoWriter = { {"logger.rootLogger.level", "INFO"},
                                          {"logger.rootLogger.writers", "CONSOLE"},
                                          {"logger.level.Test_1", "DEBUG"},
                                          {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(propsNoWriter);
    Logger *loggerNoWriter = LoggerFactory::getLogger("TestNoWriter");
    if (loggerNoWriter != NULL) {
        
        cout << "Error : logger must be null when no writer configuration" << endl;
        return -1;
    }
    delete loggerNoWriter;

    /* Test unknown writer. */
    map<string, string> propsUnknownWriter = { {"logger.rootLogger.level", "INFO"},
                                               {"logger.rootLogger.writers", "UNKNOWN"},
                                               {"logger.writer.UNKNOWN", "UnknownWriter"},
                                               {"logger.level.Test_1", "DEBUG"},
                                               {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(propsUnknownWriter);
    Logger *loggerUnknownWriter = LoggerFactory::getLogger("TestUnknownWriter");
    if (loggerUnknownWriter != NULL) {
        
        cout << "Error : logger must be null when specifying an unknown writer" << endl;
        return -1;
    }
    delete loggerUnknownWriter;
    
    /* Test bad properties for FileWriter. */
    map<string, string> propsBadFile = { {"logger.rootLogger.level", "INFO"},
                                         {"logger.rootLogger.writers", "FILE"},
                                         {"logger.writer.FILE", "FileWriter"},
                                         {"logger.writer.FILE.append", "false"},
                                         {"logger.level.Test_1", "DEBUG"},
                                         {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(propsBadFile);
    Logger *loggerBadFile = LoggerFactory::getLogger("TestBadFile");
    if (loggerBadFile != NULL) {
        
        cout << "Error : logger must be null when no filename specified for a FileWriter" << endl;
        return -1;
    }
    delete loggerBadFile;
    
    /* Test use StringWriter. */
    map<string, string> propsStringWriter = { {"logger.rootLogger.level", "INFO"},
                                              {"logger.rootLogger.writers", "STRING"},
                                              {"logger.writer.STRING", "StringWriter"},
                                              {"logger.level.Test_1", "DEBUG"},
                                              {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(propsStringWriter);
    Logger *loggerStringWriter = LoggerFactory::getLogger("TestStringWriter");
    if (loggerStringWriter != NULL) {
        
        cout << "Error : logger must be null when using StringWriter" << endl;
        return -1;
    }
    delete loggerStringWriter;

    /* Test use MultiWriter. */
    map<string, string> propsMultiWriter = { {"logger.rootLogger.level", "INFO"},
                                             {"logger.rootLogger.writers", "MULTI"},
                                             {"logger.writer.MULTI", "MultiWriter"},
                                             {"logger.level.Test_1", "DEBUG"},
                                             {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(propsMultiWriter);
    Logger *loggerMultiWriter = LoggerFactory::getLogger("TestMultiWriter");
    if (loggerMultiWriter != NULL) {
        
        cout << "Error : logger must be null when using MultiWriter" << endl;
        return -1;
    }
    delete loggerMultiWriter;

    /* Test logging. */
    map<string, string> props = { {"logger.rootLogger.level", "INFO"},
                                  {"logger.rootLogger.writers", "CONSOLE, FILE"},
                                  {"logger.writer.CONSOLE", "ConsoleWriter"},
                                  {"logger.writer.CONSOLE.format", "%d %l [%n] %f/%c %b : %m"},
                                  {"logger.writer.FILE", "FileWriter"},
                                  {"logger.writer.FILE.filename", "test_factory.log"},
                                  {"logger.writer.FILE.append", "false"},
                                  {"logger.writer.FILE.format", "%d %l [%n] : %m"},
                                  {"logger.writer.FILE.dtformat", "%Y-%m-%d"},
                                  {"logger.level.Test_1", "DEBUG"},
                                  {"logger.level.Test_2", "WARNING"} };
    PropertyConfigurator::configureFromMap(props);
    
    Logger *logger1 = LoggerFactory::getLogger("Test_1");
    if (logger1 == NULL) {
        
        cout << "Error : unable to get new logger \"Test_1\"" << endl;
        return -1;
    }

    Logger *logger2 = LoggerFactory::getLogger("Test_2");
    if (logger2 == NULL) {
        
        cout << "Error : unable to get new logger \"Test_2\"" << endl;
        return -1;
    }
    
    logger1->setLevel(INFO);
    //logger1->getWriter("CONSOLE")->setFormat("%d %l [%n] : %m");
    string emergency = "EMERGENCY";
    LOG_EMERG(logger1, "This is an " + emergency + " message");
    LOG_ALERT(logger1, "This is an ALERT message");
    LOG_CRIT(logger1, "This is a CRITICAL message");
    LOG_ERR(logger1, "This is a ERROR message");
    LOG_WARNING(logger1, "This is a WARNING message");
    LOG_NOTICE(logger1, "This is a NOTICE message");
    LOG_INFO(logger1, "This is an INFO message");
    LOG_DEBUG(logger1, "This is a DEBUG message");

    LOG_EMERG(logger2, "This is an " + emergency + " message");
    LOG_ALERT(logger2, "This is an ALERT message");
    LOG_CRIT(logger2, "This is a CRITICAL message");
    LOG_ERR(logger2, "This is a ERROR message");
    LOG_WARNING(logger2, "This is a WARNING message");
    LOG_NOTICE(logger2, "This is a NOTICE message");
    LOG_INFO(logger2, "This is an INFO message");
    LOG_DEBUG(logger2, "This is a DEBUG message");
    
    /* Get a logger a second time, we must have the same logger. */
    Logger *logger1_2 = LoggerFactory::getLogger("Test_1");
    if (logger1 != logger1_2) {
        
        cout << "Error : this is  not the same logger, first : " << logger1 << ", second : " << logger1_2 << endl;
        return -1;
    }
    
    delete logger1;
    delete logger2;
    
    return 0;
}


/**
 * @~english
 * @brief
 */
int testFormatter(void) {
    
    Location location(__FILE__, __func__, 0);
    Formatter formatter("%d %l [%n] %f/%c %b : %m");
    string want = "2016-01-06 DEBUG [NAME] logger_test.cpp/testFormatter 0 : This is a test message";
    string message = formatter.format("This is a test message", "NAME", DEBUG, "2016-01-06", location);
    
    if (want.compare(message) != 0) {
        
        cout << "Error : want \"" << want << "\", got \"" << message << endl;
        return -1;
    }
    
    return 0;
}


/**
 * @~english
 * @brief
 */
int testSetupFromCode(void) {
    
    PropertyConfigurator::clear();
    
    Logger *logger = LoggerFactory::getLogger("Test");
    ConsoleWriter cWriter;
    FileWriter fWriter("test_setup.log", true);
    logger->addWriter("CONSOLE", &cWriter);
    logger->addWriter("FILE", &fWriter);
    logger->setLevel(NOTICE);
    logger->getWriter("CONSOLE")->setFormat("%d %l [%n] %f/%c (%b) : %m");
    
    string emergency = "EMERGENCY";
    LOG_EMERG(logger, "This is an " + emergency + " message");
    LOG_ALERT(logger, "This is an ALERT message");
    LOG_CRIT(logger, "This is a CRITICAL message");
    LOG_ERR(logger, "This is a ERROR message");
    LOG_WARNING(logger, "This is a WARNING message");
    LOG_NOTICE(logger, "This is a NOTICE message");
    LOG_INFO(logger, "This is an INFO message");
    LOG_DEBUG(logger, "This is a DEBUG message");

    // Log messages will no longer be written on the console.
    logger->removeWriter("CONSOLE");
    LOG_EMERG(logger, "This is an " + emergency + " message");
    LOG_ALERT(logger, "This is an ALERT message");
    LOG_CRIT(logger, "This is a CRITICAL message");
    LOG_ERR(logger, "This is a ERROR message");
    LOG_WARNING(logger, "This is a WARNING message");
    LOG_NOTICE(logger, "This is a NOTICE message");
    LOG_INFO(logger, "This is an INFO message");
    LOG_DEBUG(logger, "This is a DEBUG message");
    
    return 0;
}

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief
 */
int fail(void) {

    cout << "FAIL" << endl;
    return 1;
}

/* -------------------------------------------------------------------------- */

int main(int argc, char **argv) {

    cout << "Testing Logger library" << endl;
    int result = 0;

    result = testFormatter();
    if (result < 0) {
        return fail();
    }
    
    /*
    result = testConsoleWriter();
    if (result < 0) {
        return fail();
    }
    */

    result = testFileWriter();
    if (result < 0) {
        return fail();
    }
    
    /*
    result = testMultiWriter();
    if (result < 0) {
        return fail();
    }
    */

    result = testStringWriter();
    if (result < 0) {
        return fail();
    }

    result = testIniParser();
    if (result < 0) {
        return fail();
    }

    result = testPropertyConfigurator();
    if (result < 0) {
        return fail();
    }
    
    result = testLoggerFactory();
    if (result < 0) {
        return fail();
    }

    result = testSetupFromCode();
    if (result < 0) {
        return fail();
    }
    
    cout << "PASS" << endl;
    return 0;
}

/* -------------------------------------------------------------------------- */
