// 
// Filename     : formatter.cpp
// Description  : Class defining a formatter for formatting a  message.
// Author       : Christophe Burki
// Version      : 1.0.0
// 

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; see the file LICENSE.  If not, write to the
// Free Software Foundation, Inc., 51 Franklin Street, Fifth
// ;; Floor, Boston, MA 02110-1301, USA.
// 

/* -------------------------------------------------------------------------- */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "formatter.h"
#include "level.h"
#include "location.h"

using namespace std;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Constructor
 */
Formatter::Formatter(const string format) {

    msgFormat = format;
}


/**
 * @~english
 * @brief Destructor
 */
Formatter::~Formatter(void) {
    
}


/**
 * @~english
 * @brief Format a message according to the format.
 *
 * @param msg The message to format.
 * @param name The name of the logger.
 * @param level The log level.
 * @param date The date.
 * @param location The log location (filename, function name and line number).
 * @return The formatted message.
 */
string Formatter::format(const string msg, const string name, const LogLevel level, const string date, const Location location) {

    string message = msgFormat;
    const map<string, string> replacement = {
        {"%d", date},
        {"%l", logLevelString[level]},
        {"%n", name},
        {"%m", msg},
        {"%f", location.getFilename()},
        {"%b", to_string(location.getLine())},
        {"%c", location.getFuncname()},
    };
    
    for (map<string, string>::const_iterator i = replacement.cbegin(); i != replacement.cend(); i++) {

        size_t pos = message.find(i->first);
        if (pos == string::npos) {
            
            continue;
        }
        
        message.replace(pos, i->first.length(), i->second);
    }

    return message;
}

/* -------------------------------------------------------------------------- */
