/* 
 * Filename     : formatter.h
 * Description  : Class defining a formatter for formatting a  message.
 * Author       : Christophe Burki
 * Version      : 1.0.0
 */

/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * ;; Floor, Boston, MA 02110-1301, USA.
 */

#ifndef BO_FORMATTER
#define BO_FORMATTER

/* -------------------------------------------------------------------------- */

#include <string>

#include "logger.h"
#include "location.h"

using std::string;

/* -------------------------------------------------------------------------- */

/**
 * @~english
 * @brief Class for formatting a message.
 */
class Formatter {

 private :
    string msgFormat;
        
 public :
    Formatter(const string format);
    ~Formatter(void);
    string format(const string msg, const string name, const LogLevel level, const string date, const Location location);
};

/* -------------------------------------------------------------------------- */

#endif
